import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:quizapp/services/auth.dart';
import 'package:quizapp/shared/bottom_nav.dart';

class ProfileScreen extends StatelessWidget {
  final AuthService auth = AuthService();

  @override
  Widget build(BuildContext context) {

    var user = Provider.of<FirebaseUser>(context);

    if (user != null) {
      return Scaffold(
      appBar: AppBar(
        title: Text(user.displayName ?? 'Guest'),
        backgroundColor: Colors.yellow,
      ),
      body: Center(
        //child: Text('Perfil')
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            if(user.photoUrl != null) 
              Container(
                width: 100,
                height: 100,
                margin: EdgeInsets.only(top: 50),
                decoration: BoxDecoration(
                  shape: BoxShape.circle,
                  image: DecorationImage(
                    image: NetworkImage(user.photoUrl)
                  ),
                ),
              ),
            Text(
              user.email ?? '', 
              style: Theme.of(context).textTheme.headline
              ),
              Spacer(),
              FlatButton(
                onPressed: () async {
                  await auth.signOut();
                  Navigator.of(context).pushNamedAndRemoveUntil('/', (route) => false);
                },
                child: Text('logout'),
                color: Colors.red,),
                Spacer(),
          ],
        )
      ),
      bottomNavigationBar: AppBottomNav(),
    );
    } else {
      return Text('not loged in');
    }
    }
}
